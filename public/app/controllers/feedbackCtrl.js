angular.module('feedbackController',['feedbackServices'])

.controller('feedCtrl', function($http, $location, $timeout, Feedback){

    var app = this;
    

    app.inputFeedback = function(Regdata){
        app.errorMsg = false;
        app.loading = true;

        Feedback.create( app.Regdata).then(function(data){
            if(data.data.success){
                app.loading = false;
                app.successMsg = data.data.message + '...Redirecting';
                $timeout(function(){
                    $location.path('/success');
                },2000)
                
            }
            else{
                app.loading = false;
                app.errorMsg = data.data.message;
            }
        })
    }
});