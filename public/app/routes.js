angular.module('appRoutes', ['ngRoute'])

.config(function($routeProvider, $locationProvider){
    $routeProvider
    .when('/', {
        templateUrl: 'app/views/pages/home.html'
    })
    .when('/about', {
        templateUrl: 'app/views/pages/about.html'
    })
    .when('/success', {
        templateUrl: 'app/views/pages/confirm.html'
    })
    .when('/feedback', {
        templateUrl: 'app/views/pages/feedback.html',
        controller: 'feedCtrl',
        controllerAs: 'feedback'
    })
    .otherwise({redirectTo: '/'});

	$locationProvider.html5Mode({
		enabled:true,
		requirebase:false
	});
});