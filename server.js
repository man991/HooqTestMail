var express = require('express');
var app  = express();
var path = require('path');
var port = process.env.PORT || 9000;

var bodyParser = require('body-parser');
var router = express.Router();
var appRoutes = require('./app/routes/api')(router);


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(__dirname + '/public'));
//Backend- Routes
app.use('/api', appRoutes);


    



app.get('*', function(req, res){
    res.sendFile(path.join(__dirname + '/public/app/views/index.html'));
})


app.get('/', function(req, res){
    res.send('Hello Putra');
});

app.listen(port, function(req, res){
    console.log('Server running on port ' + port);
});