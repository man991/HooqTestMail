var mongoose = require('mongoose');
var Schema = mongoose.Schema;

mongoose.Promise = require('bluebird');

var feedbackSchema = new Schema({
    email: String,
    title: String,
    feedback: String,
    date: String
});

module.exports = mongoose.model('Feedback', feedbackSchema);