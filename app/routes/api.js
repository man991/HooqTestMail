var feedbacks = require('../models/feedbackModel');
const nodemailer = require('nodemailer');
var mongoose = require('mongoose');
const MongoClient = require('mongodb').MongoClient;
// create reusable transporter object using the default SMTP transport
let transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 465,
    secure: true, // secure:true for port 465, secure:false for port 587
    auth: {
        user: 'testinghooq@gmail.com',
        pass: 'Indonesia17'
    }
});

// mongoose.connect('mongodb://localhost:27017/hooqfeedbackservice', function(err){
 
//         if(err){
//             console.log('Database is not connected ' + err);
//         }
//         else{
//             console.log('Database connected successfully');
//         }
//     });

    mongoose.connect('mongodb://tester:indonesia17@ds129031.mlab.com:29031/hooqfeedbackservice', function(err){
 
        if(err){
            console.log('Database is not connected ' + err);
        }
        else{
            console.log('Database connected successfully');
        }
    });

module.exports = function(router){


        router.post('/feedback', function(req, res){
        var date = new Date();
        var feed = new feedbacks();
        feed.email = req.body.email
        feed.title = req.body.title;
        feed.feedback = req.body.feedback;
        feed.date = date;


        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Mantiri Ade Putra Feedback " <testinghooq@gmail.com>', // sender address
            to: req.body.email , // list of receivers
            subject: 'Hello ✔', // Subject line
            html: '<b>'+ 'Thank you for submitting your feedback. We really appreciate it' +'</b>' // html body
        };

        
        if(req.body.email == null || req.body.email == '' ||req.body.title == null || req.body.title == '' || req.body.feedback == null || req.body.feedback == '' ){
            res.json({success: false, message: 'Please enter all the information'})
        }
        else{
            feed.save(function(err){
                if(err){
                    res.json({success: false, message: 'Input failed'})
                }
                else{
                    res.json({success: true, message: 'Submitting your feedback!'});
                    transporter.sendMail(mailOptions, (error, info) => {
                    if (error) {
                        return console.log(error);
                    }
                    console.log('Message %s sent: %s', info.messageId, info.response);
                });
                                    
                }
            });
            
        } 
    });

    return router;
}